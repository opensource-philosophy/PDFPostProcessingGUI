#!/usr/bin/env python3
import os         # für Aufgaben des Betriebssystems
import subprocess # für das Ausführen externer Programme
import platform   # für plattformunabhängige Konditionale
import sys        # für Kommandozeilenargumente
from PyQt5.QtWidgets import QApplication, QFileDialog, QCheckBox, QProgressBar, QMessageBox, QWidget, QVBoxLayout, QMainWindow, QLabel, QPushButton, QFrame, QTextEdit, QPlainTextEdit  # für GUI

# Wir definieren die Klasse OcrMyPdfGUI, von der unser Skript eine Instanz ausführen wird
class OcrMyPdfGUI(QWidget):
    def __init__(self):
        super().__init__()
        self.setWindowTitle('OCRmyPDF')
        self.setGeometry(100, 100, 400, 300)

        # Setze Pfad zur Datei, falls als Kommandozeilenargument angegeben
        if len(sys.argv) > 1:
            possible_file_path = sys.argv[1]
            if not os.path.isfile(possible_file_path):
                print('Die Datei, die Sie angegeben haben, existiert nicht!')
                sys.exit()
            elif not possible_file_path.endswith('.pdf'):
                print('Bitte geben Sie den Pfad einer .pdf-Datei an.')
                sys.exit()
            else: self.file_path = possible_file_path
        else:
            self.file_path = ''

        # Für das Sprachmenü
        self.language_options = {'deu': 'Deutsch', 'eng': 'Englisch', 'deu_frak': 'Deutsch (Fraktur)',
                                 'grc': 'Altgriechisch', 'equ': 'Formeln und Gleichungen'}

        self.language_checkboxes = []

        # Für das Extramenü (standardmäßig auf None, wird später herausgefiltert)
        self.deskew = None
        self.rotate_pages = None
        self.force_ocr = None

        # Knöpfe
        self.file_btn = None
        ocr_btn = None

        # Returncode
        self.returncode = None

        # Initialisierung des GUI
        self.initUI()

    def initUI(self):
        # Layout des GUIs
        layout = QVBoxLayout()

        # Knopf zum Auswählen der Datei
        if self.file_path == '':
            self.file_btn = self.create_button('Datei auswählen...', self.select_file)
        else:
            self.file_btn = self.create_button('Datei bereits ausgewählt (drücken zum Neuauswählen)', self.select_file)
        layout.addWidget(self.file_btn)

        ### Sprachfeld ####

        # Erstelle Checkboxen für Sprachen (Deutsch automatisch ausgewählt)
        for lang_code, lang_name in self.language_options.items():
            checkbox = QCheckBox(lang_name, self)
            if lang_code == 'deu':
                checkbox.setChecked(True)
            else:
                checkbox.setChecked(False)
            self.language_checkboxes.append((lang_code, checkbox))

        # Packe die Checkboxen ins GUI
        layout.addWidget(QLabel('Sprachen:'))  # Überschrift des Sprachfelds
        for _, checkbox in self.language_checkboxes:
            layout.addWidget(checkbox)

        ### Horizontale Linie zwischen Sprachen und weiteren Optionen ###
        line = QFrame()
        line.setFrameShape(QFrame.HLine)
        line.setFrameShadow(QFrame.Sunken)
        layout.addWidget(line)
        layout.setSpacing(10)

        ### Checkboxen für weitere Optionen ###

        # Erstelle die Checkboxen
        deskew_checkbox = self.create_checkbox('Seiten entzerren', self.toggle_deskew)
        rotate_pages_checkbox = self.create_checkbox('Seiten automatisch rotieren', self.toggle_rotate_pages)
        force_ocr_checkbox = self.create_checkbox('Alte OCR-Layer (falls vorhanden) überschreiben', self.toggle_force_ocr)

        # Packe die Checkboxen ins GUI
        other_label = QLabel('Weitere Optionen:') # Überschrift
        layout.addWidget(other_label)
        layout.addWidget(deskew_checkbox)
        layout.addWidget(rotate_pages_checkbox)
        layout.addWidget(force_ocr_checkbox)

        # Knopf zum Starten der OCR
        self.ocr_btn = self.create_button('Texterkennung starten', self.start_ocr)
        layout.addWidget(self.ocr_btn)

        # Layout setzen
        self.setLayout(layout)

    def create_button(self, text, callback):
        btn = QPushButton(text, self)
        btn.clicked.connect(callback)
        return btn

    def show_message_box(self, message):
        msg_box = QMessageBox()
        msg_box.setText(message)
        msg_box.exec_()

    def create_checkbox(self, text, callback):
        checkbox = QCheckBox(text, self)
        checkbox.stateChanged.connect(callback)
        return checkbox

    def select_file(self):
        input  = QFileDialog.getOpenFileName(self, 'Bitte wählen Sie eine PDF-Datei zur Texterkennung aus',
                                                        '', 'PDF-Dateien (*.pdf);;Alle Dateien (*)')[0]
        if os.path.isfile(input):
            self.file_path = input
            self.file_btn.setText('Datei bereits ausgewählt (drücken zum Neuauswählen)')
        self.ocr_btn.setText('Texterkennung starten')

    # Extraoptionen
    def toggle_deskew(self, state):
        self.deskew = '--deskew'

    def toggle_rotate_pages(self, state):
        self.rotate_pages = '--rotate-pages'

    def toggle_force_ocr(self, state):
        self.force_ocr = '--force-ocr'

    # Was beim Drücken des Startknopfs passiert

    def start_ocr(self):
        if not self.file_path:
            self.show_message_box('Sie haben keine Datei ausgewählt.')
            return
        # ermittle ausgewählte Sprachen
        langs = [code for code, checkbox in self.language_checkboxes if checkbox.isChecked()]
        opt_lang = '+'.join(langs)

        # Dateiname
        dir_name, file_name = os.path.split(self.file_path)
        # neuer Dateiname mit [OCR]-Präfix
        new_file_name = '[OCR] ' + file_name
        # Pfad mit neuem Dateinamen
        outfile = os.path.join(dir_name, new_file_name)
        outfile_sidecar = os.path.splitext(outfile)[0] + '.txt'
        # ocrmypdf-Befehl mit Nones (siehe Definitionen von Extraoptionen)
        ocrmypdf_command_with_nones = ['ocrmypdf',                        # OCRmyPdf
                                       '--optimize', '1',                 # sichere und verlustfreie Optimierung
                                       '--sidecar', outfile_sidecar,      # Volltext-Datei im .txt-Format
                                       '--tesseract-pagesegmode', '1',    # Spalten automatisch erkennen
                                       self.deskew,                       # Entzerren
                                       self.rotate_pages,                 # Seiten automatisch rotieren
                                       self.force_ocr,                    # OCR erzwingen (vorhandenen überschreiben)
                                       '-l', opt_lang,                    # Sprachen
                                       self.file_path,                    # Pfad der nicht OCRten Datei
                                       outfile]                           # Pfad der OCRten Datei

        # ocrmypdf-Befehl ohne Nones
        ocrmypdf_command = [arg for arg in ocrmypdf_command_with_nones if arg is not None]
        ocrmypdf_command_string = ' '.join(ocrmypdf_command)

        # Führe OCR-Befehl aus
        self.ocr_btn.setText('Texterkennung läuft...')
        self.ocr_btn.repaint()
        process = subprocess.Popen(ocrmypdf_command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
        print(f'OCR-Prozess gestartet mit diesem Befehl:\n{ocrmypdf_command_string}')

        self.returncode = process.wait()

        if self.returncode == 0: # wenn OCR erfolgreich war
            msg_box = QMessageBox()
            self.file_btn.setText('OCR bereits erzeugt (nächste Datei auswählen)')
            self.ocr_btn.setText('Texterkennung erfolgreich!')
            msg_box.setWindowTitle('OCR erfolgreich!')
            msg_box.setText('Möchten Sie die gerade erstellte Datei öffnen?')

            # Optionen
            msg_box.setStandardButtons(QMessageBox.Yes | QMessageBox.No)
            msg_box.button(QMessageBox.Yes).setText('Ja')
            msg_box.button(QMessageBox.No).setText('Nein')

            answer = msg_box.exec_()

            # Öffne die Datei im Standardviewer (Linux: evince)
            if answer == QMessageBox.Yes:
                # Öffne die OCRte Datei mit dem Standard-PDF-Viewer
                if platform.system() == 'Darwin':  # macOS
                    subprocess.Popen(['open', outfile])
                elif platform.system() == 'Windows':  # Windows
                    subprocess.Popen(['start', '', outfile], shell=True)
                else:  # Linux/Unix
                    subprocess.Popen(['evince', outfile])
        else:
            print(f'Irgendetwas ist schief gelaufen! Der returncode ist {self.returncode}.')  # oder eine entsprechende Fehlermeldung anzeigen

ocr_app = QApplication(sys.argv)

# Erstelle ein Fenster mit der gerade definierten Klasse
gui = OcrMyPdfGUI()
gui.setGeometry(100, 100, 400, 300) # Definiere Position und Größe des Fensters
gui.setWindowTitle('OCRmyPDF GUI') # Setze den Titel des Fensters

# Zeige das Fenster an
gui.show()
ocr_app.exec_()
print(gui.returncode)

sys.exit(gui.returncode)
