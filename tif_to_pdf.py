#!/usr/bin/env python3
import sys        # damit das Script Argumente annimmt
import os         # für betriebssystemspezifische Aufgaben
import shutil     # für Kopieren, Verschieben und Löschen von Dateien und Ordnern
import subprocess # für das Ausführen externer Programme
import platform   # für Unterscheidungen von Betriebssystemen

from PyQt5.QtWidgets import QFileDialog, QApplication, QMessageBox, QWidget # für GUI
from PIL import Image, ImageSequence  # für Bild-Bearbeitung (.tif und .pdf)
from pypdf import PdfMerger # um .pdf-Dateien zu mergen

# App erstellen
app = QApplication(sys.argv)

# Wenn die .tif-Datei als Argument hinterlegt ist, starte direkt, sonst frage nach einer .tif-Datei
if len(sys.argv) > 1:
    file_path = sys.argv[1]
    if not os.path.isfile(file_path):
        print('Die Datei, die Sie angegeben haben, existiert nicht!')
        sys.exit()
    elif not file_path.endswith('.tif') and not file_path.endswith('.tiff'):
        print('Bitte geben Sie den Pfad einer .tiff- oder einer .tiff-Datei an.')
        sys.exit()
else:
    options = QFileDialog.Options() # Wähle die Standardoptionen aus
    options |= QFileDialog.DontUseNativeDialog # aber nutze gleiche Oberfläche für jedes Betriebssystem

    # Fenster öffnen, file_path speichert Pfad zur .tif-Datei
    file_path, _ = QFileDialog.getOpenFileName(None, 'Wählen Sie bitte eine .tif-Datei aus', '', 'TIF files (*.tif *.tiff);;All Files (*)', options=options)
    if not file_path:
        sys.exit()

tiff_basename = os.path.basename(file_path)
tiff_basename_without_extension = os.path.splitext(os.path.basename(file_path))[0]

# Variablen für Ordner erstellen
base_dir = os.path.dirname(file_path) # Ordner, in dem die .tif-Datei sich befindet
new_dir_name = f'[Bearbeitung] {os.path.basename(tiff_basename_without_extension)}' # Name des neu zu erstellenden Ordners
new_dir_path = os.path.join(base_dir, new_dir_name) # Pfad des neu zu erstellenden Ordners

# wenn der Ordner bereits existiert, benenne ihn um
if os.path.exists(new_dir_path):
    old_dir_name = f'[alt] {new_dir_name}'
    old_dir_path = os.path.join(base_dir, old_dir_name)
    # wenn schon ein alter Ordner existiert, lösche ihn
    if os.path.exists(old_dir_path):
        shutil.rmtree(old_dir_path)
    shutil.move(new_dir_path, old_dir_path) # benenne den Ordner um

# Erstelle den neuen Ordner
os.makedirs(new_dir_path, exist_ok=False) # melde dich, wenn der alte Ordner  nicht umbenannt ist

# kopiere die .tif-Datei in den neuen Ordner
shutil.copy(file_path, os.path.join(new_dir_path, tiff_basename))

# Öffne ScanTailor
if platform.system() == 'Darwin':
    scan_tailor_command = ['/Applications/ScanTailor (Advanced).app/Contents/MacOS/ScanTailor'] # Mac
else:
    scan_tailor_command = ['scantailor'] # Linux/Windows

subprocess.run(scan_tailor_command, cwd=new_dir_path) # öffne scantailor im Bearbeitungsordner

# Pfad, in dem die bearbeiteten einzelnen .tifs gespeichert werden
out_path = os.path.join(new_dir_path, 'out/')

# Liste aller .tif-Dateien im Ordner (nur Dateinamen, ohne Pfad)
single_page_edited_tiff_bases_unsorted = [file for file in os.listdir(out_path) if file.endswith('.tif') or file.endswith('.tiff')]
single_page_edited_tiff_bases = sorted(single_page_edited_tiff_bases_unsorted)

# Berechne die Summe der Dateigrößen aller einseitigen .tif-Dateien
total_file_size = 0
for single_page_edited_tiff_base in single_page_edited_tiff_bases:
    total_file_size += os.path.getsize(os.path.join(out_path, single_page_edited_tiff_base))

# Pfad, in dem die zur .pdf umgewandelte .tif gespeichert werden soll
edited_multipage_pdf_path_no_ocr = os.path.join(base_dir, tiff_basename_without_extension) + '.pdf'

if total_file_size < 1024**3:
    print(f'Einseitige .tif-Dateien mit {round(total_file_size/(1024**2), 2)}MB insgesamt kleiner als 1GB. Erstelle eine einzige mehrseitige .tif-Datei')
    edited_and_merged_tiff_path = os.path.join(base_dir, f'[bearbeitet] {tiff_basename}')
    # Erstelle eine einzelne mehrseitige .tif-Datei
    with Image.open(out_path + single_page_edited_tiff_bases[0]) as first_image:
        images = [first_image] # Liste von Bildern erzeugen
        for single_page_edited_tiff_base in single_page_edited_tiff_bases[1:]:
            images.append(Image.open(out_path + single_page_edited_tiff_base))
            images[0].save(edited_and_merged_tiff_path, save_all=True, append_images=images[1:])
    # Konvertiere die .tif-Datei in eine .pdf-Datei
    with Image.open(edited_and_merged_tiff_path) as im:
        if im.n_frames > 1: # wenn TIF mehrseitig
            im.save(edited_multipage_pdf_path_no_ocr, save_all=True)
        else: # wenn TIF einseitig: Popup Window
            window = QWidget()
            msgBox = QMessageBox()
            msgBox.setIcon(QMessageBox.Warning)
            msgBox.setText('TIF enthält nur eine einzelne Seite!')
            msgBox.setWindowTitle('Achtung')
            msgBox.setStandardButtons(QMessageBox.Ok)
            im.save(edited_multipage_pdf_path_no_ocr)
else:
    print(f'Einseitige .tif-Dateien mit {round(total_file_size/(1024**2), 2)}MB insgesamt größer als 1GB. Wandle jede einseitige .tif-Datei einzeln um')
    # Wandle jede .einseitige .tif-Datei einzeln in eine .pdf-Datei um
    for single_page_edited_tiff_base in single_page_edited_tiff_bases:
        single_page_edited_tiff_path = os.path.join(out_path, single_page_edited_tiff_base)
        single_page_edited_tiff_basename_without_extension = os.path.splitext(os.path.basename(single_page_edited_tiff_path))[0]
        single_page_edited_pdf_path = os.path.join(out_path, single_page_edited_tiff_basename_without_extension) + '.pdf'
        with Image.open(single_page_edited_tiff_path) as im:
             im.save(single_page_edited_pdf_path, save_all=True)
    single_page_pdfs_unsorted = [os.path.join(out_path, file) for file in os.listdir(out_path) if file.endswith('.pdf')]
    print(single_page_pdfs_unsorted)
    # Führe die einseitigen .pdf-Dateien in eine einzelne zusammen
    single_page_pdfs = sorted(single_page_pdfs_unsorted)
    pdf_out_path = os.path.join(base_dir, tiff_basename_without_extension) + '.pdf'
    merger = PdfMerger()
    for single_page_pdf in single_page_pdfs:
        merger.append(single_page_pdf)
    merger.write(pdf_out_path)
    merger.close()

### Message Box: neu erzeugte .pdf-Datei öffnen? ###
msgBox_view_pdf = QMessageBox()
msgBox_view_pdf.setWindowTitle('Bearbeitung erfolgreich')
msgBox_view_pdf.setText('Möchten Sie die neu erzeugte .pdf-Datei öffnen?')
msgBox_view_pdf.setStandardButtons(QMessageBox.Yes | QMessageBox.No)
msgBox_view_pdf.button(QMessageBox.Yes).setText('Ja')
msgBox_view_pdf.button(QMessageBox.No).setText('Nein')

answer_view_pdf = msgBox_view_pdf.exec_()
# Wenn 'Ja' gedrückt wird, öffne die Datei im Standardviewer (Linux: evince)
if answer_view_pdf == QMessageBox.Yes:
    # Öffne die OCRte Datei mit dem Standard-PDF-Viewer
    if platform.system() == 'Darwin':  # macOS
        process_view_pdf = subprocess.Popen(['open', edited_multipage_pdf_path_no_ocr])
    elif platform.system() == 'Windows':  # Windows
        process_view_pdf = subprocess.Popen(['start', '', edited_multipage_pdf_path_no_ocr], shell=True)
    else: # Linux/Unix
        process_view_pdf = subprocess.Popen(['evince', edited_multipage_pdf_path_no_ocr])
    process_view_pdf.wait()

### Message Box: Soll es direkt weiter mit dem OCR gehen? ###
msgBox_ocr_pdf = QMessageBox()
msgBox_ocr_pdf.setWindowTitle('Weiter mit OCR?')
msgBox_ocr_pdf.setText('Möchten Sie direkt das Script ocr_to_pdf.py ausführen?')
msgBox_ocr_pdf.setStandardButtons(QMessageBox.Yes | QMessageBox.No)
msgBox_ocr_pdf.button(QMessageBox.Yes).setText('Ja')
msgBox_ocr_pdf.button(QMessageBox.No).setText('Nein')
answer_ocr_pdf = msgBox_ocr_pdf.exec_()

# Wenn 'ja' gedrückt wird, führe das OCR-Script mit der neu erzeugten .pdf-Datei aus
if answer_ocr_pdf == QMessageBox.Yes:
    script_directory = os.path.dirname(os.path.abspath(__file__))
    ocr_pdf_path = os.path.join(script_directory, 'ocr_pdf.py')
    process_ocr_pdf = subprocess.Popen(['python', ocr_pdf_path, edited_multipage_pdf_path_no_ocr])
    returncode_ocr_pdf = process_ocr_pdf.wait()
    print(f'Return ist {returncode_ocr_pdf}')
    if returncode_ocr_pdf == 0:
        app.quit()
    else:
        msgBox_error = QMessageBox()
        msgBox_error.setWindowTitle('Fehler')
        msgBox_error.setText('Irgendwo ist ein Fehler aufgetreten. Die App schließt sich nun.')
        msgBox_error.setIcon(QMessageBox.Information)
        msgBox_error.setStandardButtons(QMessageBox.Ok)
        msgBox_error.exec_()
        app.quit()

# Beende das Script (auch, wenn kein OCR ausgeführt wurde)
app.quit()
