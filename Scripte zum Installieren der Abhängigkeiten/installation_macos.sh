#!/bin/bash

# Wir gehen davon aus, dass ScanTailor sich schon im Anwendungsordner befindet

# Überprüfen, ob die development tools installiert sind, und installieren, wenn nötig
if type xcode-select >&- && xpath=$( xcode-select --print-path ) &&
    test -d "${xpath}" && test -x "${xpath}" ; then
    echo "Xcode Developer Tools sind bereits installiert."
else
    echo "Xcode Developer Tools werden installiert..."
    xcode-select --install
fi

# Überprüfen, ob brew installiert ist, und installieren, wenn nötig
if test ! $(which brew); then
    echo "Brew wird installiert..."
    /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
fi

# Installieren von Python, Tesseract, Ghostscript und pngquant
brew install python
brew install tesseract
brew install ghostscript
brew install pngquant
brew install wget

# python-Bibliotheken in Pfad packen (damit pip erkannt wird)
# echo 'export PATH="$HOME/Library/Python/3.11/bin:$PATH"' >> ~/.zshrc
# source ~/.zshrc

# Installieren von ocrmypdf und PyQt5 über pip
pip3.11 install ocrmypdf PyQt5

# Pakete in Pfad packen
export PATH=/usr/local/lib/python3.11/site-packages:$PATH
# Installiere Sprachdaten für Tesseract
language_data_urls=(
    "https://github.com/tesseract-ocr/tessdata/raw/main/deu.traineddata"
    "https://github.com/tesseract-ocr/tessdata/raw/main/eng.traineddata"
    "https://github.com/tesseract-ocr/tessdata/raw/main/frk.traineddata"
    "https://github.com/tesseract-ocr/tessdata/raw/main/grc.traineddata"
    "https://github.com/tesseract-ocr/tessdata/raw/main/equ.traineddata"
)

# Ändere in tessdata-Pfad
language_data_path="/usr/local/share/tessdata"

for url in "${language_data_urls[@]}"
do
    filename=$(basename "$url")
    filepath="$language_data_path/$filename"

    # Lade das Sprachpaket herunter
    wget -O "$filepath" "$url"

    # Stelle sicher, dass die Datei auch heruntergeladen wurde
    if [ ! -f "$filepath" ]; then
        echo "$filename konnte nicht heruntergeladen werden."
        exit 1
    fi
done

# Überprüfe, welche Sprachen installiert wurden
tess_langs=$(tesseract --list-langs | tr '\n' ',')

if [[ $tess_langs == *"deu"* ]]; then
    echo "Texterkennung für Deutsch installiert!"
fi

if [[ $tess_langs == *"eng"* ]]; then
    echo "Texterkennung für Englisch installiert!"
fi

if [[ $tess_langs == *"frk"* ]]; then
    echo "Texterkennung für Frakturdeutsch installiert!"
fi

if [[ $tess_langs == *"grc"* ]]; then
    echo "Texterkennung für Altgriechisch installiert!"
fi

if [[ $tess_langs == *"equ"* ]]; then
    echo "Texterkennung für Gleichungen installiert!"
fi
