# Voraussetzung: Scantailor muss im Standardpfad "C:\Program Files\ScanTailor Advanced" installiert sein!
# Das Script installiert jbig2 nicht, weil chocolatey es nicht anbietet. Das Programm wird aber nicht benötigt.

# Das Script muss mit Powershell als Administrator ausgeführt werden
# Ferner muss zeitweise die execution policy geändert werden, und zwar mit dem Befehl
# Set-ExecutionPolicy Unrestricted
# Nach erfolgreicher Ausführung des Scripts kann man sie wieder zurücksetzen mit
# Set-ExecutionPolicy -Scope Process RemoteSigned

# Wir gehen davon aus, dass Scantailor schon per GUI installiert wurde
# der Installer packt die Variable nur nicht in den Pfad
# deshalb machen wir das per Script

$envPath = [System.Environment]::GetEnvironmentVariable("Path", "Machine")
$newPath = $envPath + ";C:\Program Files\ScanTailor Advanced"
[System.Environment]::SetEnvironmentVariable("Path", $newPath, "Machine")

if (!(Get-Command choco -ErrorAction SilentlyContinue)) {
    Set-ExecutionPolicy Bypass -Scope Process -Force;
    [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.SecurityProtocolType]::Tls12;
    iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'));
}

# Programme, die über chocolatey installierbar sind
choco install python tesseract ghostscript pngquant -y

# Alle Standard-Python-Scripte direkt dem Pfad hinzufügen, damit Konsole nicht neu starten muss
$env:Path = "$env:Path;C:\Python311\Scripts"
$env:Path = "$env:Path;C:\Program Files\Tesseract-OCR"

# Programme, die wir mit pip installieren
pip install ocrmypdf PyQt5

# Tesseract Daten herunterladen
$languageDataUrls = @(
    "https://github.com/tesseract-ocr/tessdata/raw/main/deu.traineddata",
    "https://github.com/tesseract-ocr/tessdata/raw/main/eng.traineddata",
    "https://github.com/tesseract-ocr/tessdata/raw/main/deu_frak.traineddata",
    "https://github.com/tesseract-ocr/tessdata/raw/main/grc.traineddata",
    "https://github.com/tesseract-ocr/tessdata/raw/main/equ.traineddata"
)

$languageDataPath = "C:\Program Files\Tesseract-OCR\tessdata"

foreach ($url in $languageDataUrls) {
    $filename = [System.IO.Path]::GetFileName($url)
    $filepath = Join-Path $languageDataPath $filename

    # Lade das Sprachpaket herunter
    Invoke-WebRequest $url -OutFile $filepath

    # Stelle sicher, dass die Datei auch heruntergeladen wurde
    if (-not (Test-Path $filepath)) {
        Write-Error "$filename konnte nicht heruntergeladen werden."
        break
    }
}

# Stelle sicher, dass tesseract das Sprachpaket nutzen kann
$tessLangs = (tesseract --list-langs) -join ","
if ($tessLangs -like "*deu*") {
    Write-Host "Texterkennung für Deutsch installiert!"
}
if ($tessLangs -like "*eng*") {
    Write-Host "Texterkennung für Englisch installiert!"
}
if ($tessLangs -like "*deu_frak*") {
    Write-Host "Texterkennung für Frakturdeutsch installiert!"
}
if ($tessLangs -like "*grc*") {
    Write-Host "Texterkennung für Altgriechisch installiert!"
}
if ($tessLangs -like "*equ*") {
    Write-Host "Texterkennung für Gleichungen installiert!"
}
